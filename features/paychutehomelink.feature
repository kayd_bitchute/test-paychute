Feature: Paychute Homepage Link
  Background:
    Given I launch browser
    When I open the paychute homepage
  @ignore
  Scenario: Check sign in with bitchute button
    When I click sign in with bitchute
    Then User must successfully open Authorize Paychute page
  @ignore
  Scenario: Check log in button
    When I click log in button
    Then User must successfully open Login page

    Scenario: Check Terms & Conditions
      When I click terms & condition link
      Then User must successfully open Terms & Condition

      @ignore
      Scenario: Check Privacy Policy
        When I click privacy policy link
        Then User must successfully open Privacy Policy

        Scenario: Check Community Guidelines
          When I click community guidelines link
          Then User must successfully open Community Guidelines

          Scenario: Check Contact Us
            When I click contact us link
            Then User must successfully open Contact Us

            Scenario: Check About us
              When I click about us link
              Then User must successfully open Support Bitchute

              Scenario: Check FAQ
                When I click faq link
                Then User must successfully open Support Bitchute

                Scenario: Check Knowledge Base
                  When I click knowledge base link
                  Then User must successfully open Support Bitchute